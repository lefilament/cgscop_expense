# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class CGScopExpense(models.Model):
    _inherit = 'hr.expense'

    expense_gap = fields.Float(
        string="Plafond de dépense",
        related="product_id.expense_gap")
    timesheet_id = fields.Many2one(
        comodel_name="account.analytic.line",
        string="Ligne de Temps",
        ondelete="restrict")
    analytic_account_id = fields.Many2one(
        related="timesheet_id.account_id",
        string="Code Activité UR",
        store=True)
    coop_id = fields.Many2one(
        related="timesheet_id.partner_id",
        string="Contact",
        store=True)
    ur_financial_system_id = fields.Many2one(
        related="timesheet_id.ur_financial_system_id",
        string='Dispositif Financier',
        store=True)
    expense_formula = fields.Selection(
        related='product_id.expense_formula')
    quantity_computed = fields.Float(
        string="Quantité",
        compute="_compute_values")
    unit_amount_computed = fields.Float(
        string="Prix",
        compute="_compute_values")

    # ------------------------------------------------------
    # Computed Fields
    # ------------------------------------------------------
    @api.depends('quantity', 'unit_amount')
    def _compute_values(self):
        self.quantity_computed = self.quantity
        self.unit_amount_computed = self.unit_amount

    # ------------------------------------------------------
    # Onchange Fields
    # ------------------------------------------------------
    @api.onchange('total_amount')
    def onchange_total_amount(self):
        for exp in self:
            if exp.product_id.expense_gap > 0:
                if exp.total_amount > exp.product_id.expense_gap:
                    return {
                        'warning': {
                            'title': "Plafond dépassé",
                            'message': "Attention, le montant est supérieur \
                                        au plafond autorisé"}}

    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id.expense_formula == 'fixed_price':
            self.unit_amount = self.product_id.standard_price
            self.update({
                'unit_amount': self.product_id.standard_price
                })
        elif self.product_id.expense_formula == 'fixed_rate':
            self.quantity = 1.0
            self.product_id.unit_amount = self.product_id.standard_price
