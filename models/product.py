# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class CGScopProductTemplate(models.Model):
    _inherit = 'product.template'

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    expense_gap = fields.Float("Plafond de dépense")
    expense_formula = fields.Selection(
        [('free', 'Libre'),
         ('fixed_rate', 'Forfait'),
         ('fixed_price', 'Prix fixe')],
        string="Type de calcul",
        default="free",
        help="""
            Définit l'option de calcul dans la note de frais :
             - Libre : permet à l'utilisateur de définir le prix et la quantité
             - Forfait : l'utilisateur n'a pas la possibilité de modifier le
            prix ni la quantité égale à 1 par défaut
             - Prix fixe : le prix est fixe, mais l'utilisateur peut modifier
            la quantité (pour les km par exemple)
            """)
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)


class CGScopProductProduct(models.Model):
    _inherit = 'product.product'

    @api.one
    @api.constrains('expense_formula', 'standard_price')
    def _check_expense_formula(self):
        if self.expense_formula != 'free' and self.standard_price == 0.0:
            raise ValidationError(
                "Le coût doit être supérieur à 0 si la formule de \
                calcul n'est pas Libre")
