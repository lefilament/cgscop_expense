# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class ScopHrTimesheetExp(models.Model):
    _inherit = "account.analytic.line"

    expense_ids = fields.One2many(
        comodel_name='hr.expense',
        inverse_name='timesheet_id',
        string='Dépenses associées')
    expense_amount = fields.Monetary(
        string="Montant des dépenses",
        currency_field='company_currency_id',
        compute="_compute_expense_amount")
    company_currency_id = fields.Many2one(
        'res.currency',
        related='company_id.currency_id',
        string="Company Currency",
        readonly=True)

    # ------------------------------------------------------
    # Compute Functions
    # ------------------------------------------------------
    @api.multi
    def _compute_expense_amount(self):
        for ts in self:
            ts.expense_amount = sum(ts.expense_ids.mapped('total_amount'))

    # ------------------------------------------------------
    # Button Functions
    # ------------------------------------------------------
    @api.multi
    def create_expense(self):
        for ts in self:
            return {
                "type": 'ir.actions.act_window',
                "res_model": 'hr.expense',
                "views": [[self.env.ref('hr_expense.hr_expense_view_form').id, "form"]],
                "view_mode": "form",
                'context': {
                    'default_timesheet_id': ts.id,
                    'default_coop_id': ts.partner_id.id,
                    'default_account_analytic_id': ts.project_id.id,
                    'default_ur_financial_system_id': ts.ur_financial_system_id.id,
                    'default_date': ts.date,
                },
            }

    @api.multi
    def show_expense(self):
        for ts in self:
            return {
                "type": "ir.actions.act_window",
                "res_model": "hr.expense",
                "views": [[False, "tree"], [False, "form"], [False, "pivot"]],
                "domain": [['timesheet_id', '=', self.id]],
                "name": "Détail Dépenses",
                'context': {
                    'default_timesheet_id': ts.id,
                    'default_coop_id': ts.partner_id.id,
                    'default_account_analytic_id': ts.project_id.id,
                    'default_ur_financial_system_id': ts.ur_financial_system_id.id,
                    'default_date': ts.date,
                },
            }
