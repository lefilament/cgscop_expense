{
    "name": "CG SCOP - Frais",
    "summary": "Adaptation Frais pour la CG Scop",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "license": "AGPL-3",
    "application": False,
    "depends": [
        'hr_expense',
        'l10n_fr',
        'hr_timesheet',
        'cgscop_timesheet'
    ],
    "data": [
        "security/ir.model.access.csv",
        "security/security_rules.xml",
        "views/hr_expense.xml",
        "views/hr_expense_sheet.xml",
        "views/hr_timesheet.xml",
        "views/product.xml",
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'installable': True,
    'auto_install': False,
}
