.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=================
CG SCOP - Expense
=================

Description
===========

Ce module permet hérite le module *hr_expense* d'Odoo pour ajouter les fonctionnalités suivantes : 

* modification de la form vue *hr_expense*
* ajout d'un journal de note de frais
* modification du modèle *hr_expense_sheet* pour mettre le journal de NdF par défaut


Credits
=======

Funders
------------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>
* Benjamin Rivier <benjamin@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
